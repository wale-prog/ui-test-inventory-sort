# UI Sort task

> This repository contains the automated UI task provided in the assessment.


## Built With

- JavaScript
- TypeScript
- Playwright
- Allure-Playwright
- Git & GitLab


### Prerequisites

- Have an IDE installed on your computer to be able to view the code locally.
- Node.js v16+

### Setup

- Open a command line terminal (Git bash preferrably) and navigate to a directory where you would like to save the working directory using cd.
- Clone the repository with
```bash
git clone https://gitlab.com/wale-prog/ui-test-inventory-sort.git
```
- To get the curent working tree
```bash
 git fetch --all
 ```
  then checkout to "main" branch.
- Install dependecies by running
```bash
 npm install
 ```
#### Running tests locally and generating reports 
To run the test and generate report run the following command:
```bash
npm run test-with-report
```
I have implemented allure reports and it will display the test report after test run on the browser.

Alternatively, you can view the default playwright report by running the following command
```bash
npm run pw-report
```
#### Running tests using GitLab CI
The CI/CD pipleline i configured uing the `.gitlab-ci.yml` file to run the automated test on Gitlab using the playwright Docker runner. The CI job can be triggered upon either a merge or a commit to the main branch. Alternatively, you can trigger the pipeline manually from the GitLab UI


## Author

👤 **Olawale Olapetan**

- GitHub: [@wale-prog](https://github.com/wale-prog)
- Twitter: [@Wale_Petan](https://twitter.com/wale_Petan)
- LinkedIn: [@walepetan](https://www.linkedin.com/in/walepetan/)
- GitLab: [@wale-prog](https://gitlab.com/wale-prog)

## 🤝 Contributing

Contributions, issues, and feature requests are welcome!

## Show your support

Give a ⭐️ if you like this project!

## Acknowledgments

- [Playwright](https://playwright.dev/)
- [Allure](https://docs.qameta.io/allure/)
- [GitLab CI/CD](https://docs.gitlab.com/ee/ci/)
- [Docker](https://www.docker.com/)
- [Node.js](https://nodejs.org/)
- [NPM](https://www.npmjs.com/)
- [TypeScript](https://www.typescriptlang.org/)
- [Visual Studio Code](https://code.visualstudio.com/)

