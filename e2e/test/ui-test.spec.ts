import { expect, test, Page } from '@playwright/test'
import { loginPage } from '../Pages/LoginPage';
import { productPage } from '../Pages/ProductPage';

test.describe('Inventory sort test', () => {

  let LoginPage: loginPage;
  let ProductPage: productPage;
  let page: Page;
  const productNames: Array<string> = [];

  test.beforeAll('Set up all pages required', async({ browser }) => {
    page = await browser.newPage();
    LoginPage = new loginPage(page)
    ProductPage = new productPage(page)
  })

  test.beforeEach('Validate user is able to login', async() => {
    await LoginPage.navigateToURL('/')
    await LoginPage.login('standard_user', 'secret_sauce')
    expect(await page.title()).toBe('Swag Labs')
    const unsortedProductName = await ProductPage.getProductNames();
    for(let product of unsortedProductName) {
      productNames.push(await product.innerText())
    }
  })
  test('Validate that the items are sorted in ascending order (A-Z)', async() => {
    const sortedProductName = [...productNames].sort()
    for(let i = 0; i < sortedProductName.length; i += 1) {
      expect(productNames[i]).toEqual(sortedProductName[i])
    }
  
  })
  test('Validate that the items are sorted in descending order(Z-A)', async() => {
    await ProductPage.reverseOrder()
    const reversedProductNames = productNames.reverse();
    const unsortedProductName = await ProductPage.getProductNames();
    for(let i = 0; i < unsortedProductName.length; i++) {
      expect(await unsortedProductName[i].innerText()).toEqual(reversedProductNames[i])
    }
  })
})