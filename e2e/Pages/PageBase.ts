import { type Locator, type Page } from '@playwright/test';

export class PageBase {
  readonly page: Page

  constructor(page: Page) {
    this.page = page
  }

  async enterText(element: Locator, text: string) {
    await element.fill(text)
  }

  async click(element: Locator) {
    await element.click()
  }
}