import { type Locator, type Page } from '@playwright/test';
import { PageBase } from './PageBase';


export class productPage extends PageBase{

  private reverseProduct: Locator

  constructor(page: Page) {
    super(page)
    this.reverseProduct = page.locator('[data-test="product-sort-container"]')
  }

  async getProductNames() {
    return await this.page.$$('.inventory_item_name')
  }
  async reverseOrder() {
    await this.reverseProduct.selectOption('za')
  }
}