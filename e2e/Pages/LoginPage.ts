import { type Locator, type Page } from '@playwright/test';
import { PageBase } from './PageBase';

export class loginPage extends PageBase {
  readonly username_field: Locator;
  readonly password_field: Locator;
  readonly login_button: Locator;

  constructor(page: Page) {
    super(page)
    this.username_field = page.locator('[data-test="username"]')
    this.password_field = page.locator('[data-test="password"]')
    this.login_button = page.locator('[data-test="login-button"]')
  }

  async navigateToURL(url: string) {
    await this.page.goto(url)
  }

  async login(username: string, password: string) {
    await this.enterText(this.username_field, username)
    await this.enterText(this.password_field, password)
    await this.click(this.login_button)
  }
}